#!/usr/bin/env node

function printUsage () {
    console.error(
        'Usage:\n' +
        ' ./filter <field> eq|gt|lt <value>\n' +
        ' ./filter <field> contains|ncontains|neq <value1> [<value2> ...]'
    )
    process.exit(1)
}

process.chdir(__dirname)
process.stdout.on('error', () => {})

const args = process.argv.slice(2)
if (args.length < 3) printUsage()

const conditionMatch = args[1].match(/^contains|eq|gt|lt|ncontains|neq$/)
if (!conditionMatch) printUsage()

let filterFunction

const condition = conditionMatch[0]
if (condition.match(/^eq|gt|lt$/)) {
    (() => {

        if (args.length > 3) printUsage()

        const value = args[2]
        if (condition == 'eq') {
            filterFunction = fieldValue => fieldValue == value
        } else if (condition == 'gt') {
            filterFunction = fieldValue => fieldValue > value
        } else if (condition == 'lt') {
            filterFunction = fieldValue => fieldValue < value
        }

    })()
} else {
    (() => {
        const values = args.slice(2)
        if (condition == 'contains') {
            filterFunction = fieldValue => {

                const type = typeof fieldValue
                if (type == 'number') fieldValue = String(fieldValue)
                else if (type != 'string') return false

                for (const i in values) {
                    if (fieldValue.indexOf(values[i]) == -1) return false
                }
                return true

            }
        } else if (condition == 'ncontains') {
            filterFunction = fieldValue => {

                const type = typeof fieldValue
                if (type == 'number') fieldValue = String(fieldValue)
                else if (type != 'string') return false

                for (const i in values) {
                    if (fieldValue.indexOf(values[i]) != -1) return false
                }
                return true

            }
        } else {
            filterFunction = fieldValue => {
                for (const i in values) {
                    if (fieldValue == values[i]) return false
                }
                return true
            }
        }
    })()
}

const fieldParts = args[0].split(/\./)

const FindFieldValue = require('./lib/FindFieldValue.js')

require('./lib/ReadLineByLine.js')(line => {
    if (filterFunction(FindFieldValue(line, fieldParts))) {
        console.log(JSON.stringify(line))
    }
})
