#!/usr/bin/env node

process.chdir(__dirname)
process.stdout.on('error', () => {})

const args = process.argv.slice(2)
if (args.length != 1) {
    console.error('Usage: ./group <field>')
    process.exit(1)
}

const FindFieldValue = require('./lib/FindFieldValue.js')

const fieldParts = args[0].split(/\./)

require('./lib/ReadAllLines.js')(allLines => {

    const groups = Object.create(null)
    const groupKeys = []
    allLines.forEach(line => {
        const key = FindFieldValue(line, fieldParts)
        if (!groups[key]) {
            groups[key] = 0
            groupKeys.push(key)
        }
        groups[key]++
    })

    const groupItems = []
    groupKeys.forEach(key => {
        console.log(JSON.stringify({
            key: key,
            value: groups[key],
        }))
    })

})
