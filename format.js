#!/usr/bin/env node

process.chdir(__dirname)
process.stdout.on('error', () => {})

const args = process.argv.slice(2)
if (args.length != 1) {
    console.error('Usage: ./format <template>')
    process.exit(1)
}

const FindFieldValue = require('./lib/FindFieldValue.js')

const template = args[0]

require('./lib/ReadLineByLine.js')(line => {
    console.log(
        template.replace(/\{(.*?)\}/g, (a, fieldName) => {
            const fieldParts = fieldName.split(/\./)
            return FindFieldValue(line, fieldParts)
        })
    )
})
