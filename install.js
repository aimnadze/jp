#!/usr/bin/env node

process.chdir(__dirname)

const fs = require('fs')

const binDir = '/usr/bin'

const target = binDir + '/jp'
try {
    fs.unlinkSync(target)
} catch (e) {
    const code = e.code
    if (code === 'EACCES') {
        console.log('ERROR: Permission denied')
        process.exit(1)
    }
    if (code !== 'ENOENT') throw e
}

try {
    fs.symlinkSync(__dirname + '/index.js', target)
} catch (e) {
    const code = e.code
    if (code === 'EACCES') {
        console.log('ERROR: Permission denied')
        process.exit(1)
    }
    throw e
}
