#!/usr/bin/env node

process.chdir(__dirname)
process.stdout.on('error', () => {})

const util = require('util')

require('./lib/ReadLineByLine.js')(line => {
    console.log(util.inspect(line, { depth: null }))
})
