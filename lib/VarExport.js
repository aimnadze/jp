module.exports = value => {

    function var_export (value, prefix) {

        if (value === null) return 'null'
        if (['boolean', 'number'].indexOf(typeof value) !== -1) {
            return JSON.stringify(value)
        }

        if (typeof value === 'string') {
            return JSON.stringify(value).replace(/'/g, '\\\'').replace(/^"/, "'").replace(/"$/, "'")
        }

        const lines = (() => {

            if (value instanceof Array) {
                return [
                    '[',
                    ...value.map(value => (
                        '    ' + var_export(value, prefix + '    ') + ','
                    )),
                    ']',
                ]
            }

            return [
                '{',
                ...Object.keys(value).map(key => (
                    '    ' + (() => {
                        if (/\W/.test(key)) return var_export(key)
                        return key
                    })() + ': ' + var_export(value[key], prefix + '    ') + ','
                )),
                '}',
            ]

        })()

        return lines.join('\n' + prefix)

    }

    return var_export(value, '')

}
