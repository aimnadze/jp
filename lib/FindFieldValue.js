module.exports = (line, fieldParts) => {
    let value = line
    for (let i = 0; i < fieldParts.length; i++) {
        if (value instanceof Object) value = value[fieldParts[i]]
        else return null
    }
    if (value === undefined) return null
    return value
}
