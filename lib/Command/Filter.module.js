module.exports = app => args => {

    function printUsage () {
        console.error(
            'Usage:\n' +
            '  jp filter <field> eq|gt|lt <value>\n' +
            '  jp filter <field> contains|ncontains|neq <value1> [<value2> ...]'
        )
        process.exit(1)
    }

    if (args.length < 3) printUsage()

    const conditionMatch = args[1].match(/^contains|eq|gt|lt|ncontains|neq$/)
    if (!conditionMatch) printUsage()

    let filterFunction

    const condition = conditionMatch[0]
    if (condition.match(/^eq|gt|lt$/)) {
        (() => {

            if (args.length > 3) printUsage()

            const value = args[2]
            if (condition == 'eq') {
                filterFunction = fieldValue => fieldValue == value
            } else if (condition == 'gt') {
                filterFunction = fieldValue => fieldValue > value
            } else if (condition == 'lt') {
                filterFunction = fieldValue => fieldValue < value
            }

        })()
    } else {
        (() => {
            const values = args.slice(2)
            if (condition == 'contains') {
                filterFunction = fieldValue => {

                    const type = typeof fieldValue
                    if (type == 'number') fieldValue = String(fieldValue)
                    else if (type != 'string') return false

                    for (const i in values) {
                        if (fieldValue.indexOf(values[i]) == -1) return false
                    }
                    return true

                }
            } else if (condition == 'ncontains') {
                filterFunction = fieldValue => {

                    const type = typeof fieldValue
                    if (type == 'number') fieldValue = String(fieldValue)
                    else if (type != 'string') return false

                    for (const i in values) {
                        if (fieldValue.indexOf(values[i]) != -1) return false
                    }
                    return true

                }
            } else {
                filterFunction = fieldValue => {
                    for (const i in values) {
                        if (fieldValue == values[i]) return false
                    }
                    return true
                }
            }
        })()
    }

    const fieldParts = args[0].split(/\./)

    app.ReadLineByLine(line => {
        if (filterFunction(app.FindFieldValue(line, fieldParts))) {
            console.log(JSON.stringify(line))
        }
    })

}
