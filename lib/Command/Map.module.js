module.exports = app => args => {

    if (args.length !== 1) {
        console.error('Usage: jp map {field}')
        process.exit(1)
    }

    const fieldParts = args[0].split(/\./)

    app.ReadLineByLine(line => {
        console.log(JSON.stringify(app.FindFieldValue(line, fieldParts)))
    })

}
