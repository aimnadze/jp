module.exports = app => args => {

    if (args.length != 1) {
        console.error('Usage: jp group <field>')
        process.exit(1)
    }

    const fieldParts = args[0].split(/\./)

    app.ReadAllLines(allLines => {

        const groups = Object.create(null)
        const groupKeys = []
        allLines.forEach(line => {
            const key = app.FindFieldValue(line, fieldParts)
            if (!groups[key]) {
                groups[key] = 0
                groupKeys.push(key)
            }
            groups[key]++
        })

        const groupItems = []
        groupKeys.forEach(key => {
            console.log(JSON.stringify({
                key: key,
                value: groups[key],
            }))
        })

    })

}
