module.exports = app => args => {

    function printUsage () {
        console.error('Usage: jp sort <field> [desc]')
        process.exit(1)
    }

    let sortDirection
    if (args.length == 2) {
        if (args[1] != 'desc') printUsage()
        sortDirection = -1
    } else if (args.length == 1) {
        sortDirection = 1
    } else {
        printUsage()
    }

    const fieldParts = args[0].split(/\./)

    app.ReadAllLines(allLines => {

        allLines.sort((a, b) => {
            const aValue = app.FindFieldValue(a, fieldParts)
            const bValue = app.FindFieldValue(b, fieldParts)
            if (aValue == bValue) return 0
            if (aValue > bValue) return sortDirection
            return -sortDirection
        })

        allLines.forEach(line => {
            console.log(JSON.stringify(line))
        })

    })

}
