module.exports = app => args => {

    if (args.length !== 0) {
        console.error('Usage: jp pretty')
        process.exit(1)
    }

    app.ReadLineByLine(line => {
        console.log(JSON.stringify(line, null, 4))
    })

}
