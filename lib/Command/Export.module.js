module.exports = app => args => {

    if (args.length !== 0) {
        console.error('Usage: jp export')
        process.exit(1)
    }

    app.ReadLineByLine(line => {
        console.log(app.VarExport(line))
    })

}
