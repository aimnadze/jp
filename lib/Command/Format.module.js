module.exports = app => args => {

    if (args.length != 1) {
        console.error('Usage: jp format <template>')
        process.exit(1)
    }

    const template = args[0]

    app.ReadLineByLine(line => {
        console.log(
            template.replace(/\{(.*?)\}/g, (a, fieldName) => {
                const fieldParts = fieldName.split(/\./)
                return app.FindFieldValue(line, fieldParts)
            })
        )
    })

}
