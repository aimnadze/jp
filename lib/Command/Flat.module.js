module.exports = app => args => {

    if (args.length !== 0) {
        console.error('Usage: jp flat')
        process.exit(1)
    }

    app.ReadLineByLine(line => {
        ;(line instanceof Array ? line : [line]).forEach(item => {
            console.log(JSON.stringify(item))
        })
    })

}
