module.exports = callback => {

    function flush () {
        const lines = stringData.split(/\n/)
        const lastLine = lines.pop()
        lines.forEach(receive)
        stringData = lastLine
    }

    function receive (line) {
        lineNumber++
        let object
        try {
            object = JSON.parse(line)
        } catch (e) {
            console.error('ERROR: Invalid JSON document on line ' + lineNumber)
            return
        }
        if (object !== undefined) allLines.push(object)
    }

    const stream = process.stdin
    const allLines = []
    let stringData = ''
    let lineNumber = 0

    stream.on('data', data => {
        stringData += data.toString('utf8')
        flush()
    })

    stream.on('end', () => {
        flush()
        if (stringData.length !== 0) receive(stringData)
        callback(allLines)
    })

}
