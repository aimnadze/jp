module.exports = app => () => {

    function printUsage () {
        console.error(
            'Usage: jp <command>\n' +
            '\n' +
            'Available commands:\n' +
            Object.keys(commands).map(command => '  ' + command).join('\n')
        )
        process.exit(1)
    }

    process.stdout.on('error', () => {})

    const commands = Object.create(null)
    commands.export = app.Command.Export
    commands.filter = app.Command.Filter
    commands.flat = app.Command.Flat
    commands.format = app.Command.Format
    commands.group = app.Command.Group
    commands.map = app.Command.Map
    commands.pretty = app.Command.Pretty
    commands.sort = app.Command.Sort

    const args = process.argv
    if (args.length < 2) printUsage()

    const command = commands[args[2]]
    if (command === undefined) printUsage()

    command(args.slice(3))

}
