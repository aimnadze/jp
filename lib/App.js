const fs = require('fs')
const path = require('path')

module.exports = () => {

    function scan (dir) {
        const object = {}
        fs.readdirSync(root + dir).forEach(name => {

            const item_path = root + dir + '/' + name
            if (fs.statSync(item_path).isDirectory()) {
                object[name] = scan(dir + '/' + name)
                return
            }

            const module = require(item_path)
            const basename = path.basename(name, '.js')

            const module_name = path.basename(basename, '.module')
            if (module_name === basename) {
                object[basename] = module
                return
            }

            module_items.push(app => {
                object[module_name] = module(app)
            })

        })
        return object
    }

    const root = __dirname
    const module_items = []
    const modules = scan('')

    const app = {}

    module_items.forEach(item => {
        item(app)
    })
    Object.assign(app, modules)

    return app

}
