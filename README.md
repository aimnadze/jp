JP
==

JP is a set of command line tools for JSON log file processing.
JP commands read JSON documents from standard input one on each
line and apply an operation. Below are subcommands of JP:

* `export` - prints each input document as a JavaScript object literal.
* `filter` - echoes back the input documents that match a given criteria.
* `flat` - echoes each element of an array on its own line.
* `format` - echoes a template filled with the values from each input document.
* `group` - groups input documents by a given field and
    echoes the number of documents in each group.
* `map` - echoes the value of a given field for each input document.
* `pretty` - pretty-prints each input document.
* `sort` - sorts the list of input documents by a given field.

Examples
--------

```
$ cat log_file | jp export
```

```
$ cat log_file | jp filter some_field eq some_value
$ cat log_file | jp filter some_field neq some_value
$ cat log_file | jp filter some_field gt some_value
$ cat log_file | jp filter some_field lt some_value
$ cat log_file | jp filter some_field contains some_value1 [some_value2] ...
$ cat log_file | jp filter some_field ncontains some_value1 [some_value2] ...
```

```
$ cat log_file | jp flat
```

```
$ cat log_file | jp format '{some_field1} {some_field2.subfield}'
```

```
$ cat log_file | jp group some_field
```

```
$ cat log_file | jp map some_field
```

```
$ cat log_file | jp pretty
```

```
$ cat log_file | jp sort some_field
```

Installation
------------

Run `./install.js` as a super user.
